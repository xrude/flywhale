# FlyWhale

#### 介绍
FlyWhale(飞鲸)低代码开发平台, 是基于php的低代码开发平台，它拥有在线数据开发和在线表单设计功能，帮助您快速的构建应用，而无需或仅需要编写少量的代码。

#### 软件架构
FlyWhale 是基于thinkphp6 + layui开发的可视化低代码快开平台。

#### QQ交流群
QQ：1011319691

# 一睹为快
![](./screenshot/install.png) 
![](./screenshot/index.png) 
![](./screenshot/online.png) 
![](./screenshot/form.png) 

#### 安装教程

1.  下载 FlyWhale https://gitee.com/nickbai/flywhale.git
2.  将根目录指向 `public`, 并配置好`域名`
3.  访问`域名` 根据步骤指引安装系统（此处不懂的可以去查看 pearadmin）
4.  安装完毕后，访问域名就可以登录进行操作了

#### 使用说明

可以参考B站我演示的如何开发一个简单的《体温上报》功能，快速入门。
https://www.bilibili.com/video/BV1Vb4y1b7kU/

#### 鸣谢

感谢如下开源项目，正是这些开源项目，才有了FlyWhale的开源。

1.  thinkphp 框架 [https://thinkphp.cn](https://thinkphp.cn)
2.  pearadmin [http://www.pearadmin.com/](http://www.pearadmin.com/)
3.  ayq-layui-form-designer [https://gitee.com/ayq947/ayq-layui-form-designer](https://gitee.com/ayq947/ayq-layui-form-designer)
