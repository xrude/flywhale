<?php

namespace app\common\flydesigner;

class {{$name}}Event
{
    /**
     * 重新整理列表数据
     * @param $list
     * @return mixed
     */
    public function redoListParam($list)
    {
        return $list;
    }

    /**
     * 重新整理新增的参数
     * @param $param
     * @return mixed
     */
    public function redoAddParam($param)
    {
        return $param;
    }

    /**
     * 新增操作之前的逻辑钩子
     * @param $param
     * @return array
     */
    public function beforeAdd($param)
    {
        return dataReturn(0, 'success');
    }

    /**
     * 新增操作之后的逻辑钩子
     * @param $param
     * @return array
     */
    public function afterAdd($param)
    {
        return dataReturn(0, 'success');
    }

    /**
     * 重新整理编辑的参数
     * @param $param
     * @return mixed
     */
    public function redoEditParam($param)
    {
        return $param;
    }

    /**
     * 编辑操作之前的逻辑钩子
     * @param $param
     * @return array
     */
    public function beforeEdit($param)
    {
        return dataReturn(0, 'success');
    }

    /**
     * 编辑操作之后的逻辑钩子
     * @param $param
     * @return array
     */
    public function afterEdit($param)
    {
        return dataReturn(0, 'success');
    }

    /**
     * 删除操作之前的逻辑钩子
     * @param $id
     * @return array
     */
    public function beforeDel($id)
    {
        return dataReturn(0, 'success');
    }

    /**
     * 删除操作之后的逻辑钩子
     * @param $id
     * @return array
     */
    public function afterDel($id)
    {
        return dataReturn(0, 'success');
    }
}