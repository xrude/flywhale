<?php

namespace app\common\model;

use think\Model;

class HomeAuthApply extends Model
{
    /**
     * 获取申请列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getApplyList($where, $limit)
    {
        try {

            $list = $this->where($where)->order('status','asc')->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0,'success', $list);
    }
}