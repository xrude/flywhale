<?php
declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\common\model\AdminCar;
use app\common\model\AdminCarApply;
use think\facade\Request;
use app\common\service\AdminCar as S;
use app\common\model\AdminCar as M;
use think\facade\Session;

class Car extends  \app\admin\controller\Base
{
    protected $carType = [
        1 => '轿车',
        2 => 'SUV',
        3 => 'MPV',
        4 => '皮卡',
        5 => '货车'
    ];

    protected $status = [
        1 => '正常',
        2 => '维修中',
        3 => '保养中',
        4 => '报废'
    ];

    protected $middleware = ['AdminCheck','AdminPermission'];

    // 列表
    public function index()
    {
        if (Request::isAjax()) {
            return $this->getJson(M::getList());
        }
        return $this->fetch('', [
            'car_type' => json_encode($this->carType),
            'status'   => json_encode($this->status)
        ]);
    }

    // 添加
    public function add()
    {
        if (Request::isAjax()) {
            return $this->getJson(S::goAdd(Request::post()));
        }

        return $this->fetch('', [
            'car_type' => $this->carType,
            'status' => $this->status
        ]);
    }

    // 编辑
    public function edit($id)
    {
        if (Request::isAjax()) {
            return $this->getJson(S::goEdit(Request::post(),$id));
        }
        return $this->fetch('',[
            'model' => M::find($id),
            'car_type' => $this->carType,
            'status' => $this->status
        ]);
    }

    // 状态
    public function status($id)
    {
        return $this->getJson(S::goStatus(Request::post('status'),$id));
    }

    // 删除
    public function remove($id)
    {
        return $this->getJson(S::goRemove($id));
    }

    // 批量删除
    public function batchRemove()
    {
        return $this->getJson(S::goBatchRemove(Request::post('ids')));
    }

    // 车辆预约
    public function subscribe()
    {
        if (request()->isAjax()) {

            $date = input('param.date');
            $carId = input('param.car_id');
            $where[] = ['status', '=', 2];

            if (!empty($date)) {
                $where[] = ['start', '>', $date . '-01'];
                $where[] = ['end', '<', date('Y-m-t', strtotime($date))];
            }

            if (!empty($carId)) {
                $where[] = ['car_id', '=', $carId];
            }

            $res = AdminCarApply::where($where)->select();
            return jsonReturn(0, 'success', $res);
        }

        $car =  M::select();

        return $this->fetch('', [
            'car' => $car,
            'car_type' => $this->carType,
            'status' => $this->status,
            'carType' => json_encode($this->carType)
        ]);
    }

    // 提交预约
    public function doSubscribe()
    {
        if (\request()->isPost()) {

            $param = input('post.');
            $now = date('Y-m-d H:i:s');
            if ($param['start'] < $now || $param['end'] < $now) {
                return jsonReturn(-1, '日期不得早于今天');
            }

            if ($param['start'] > $param['end']) {
                return jsonReturn(-2, '结束日期不得早于开始日期');
            }

            $carInfo = AdminCar::where('id', $param['car_id'])->find();
            if (empty($carInfo)) {
                return jsonReturn(-3, '该车不存在');
            }

            if ($carInfo['status'] != 1) {
                return jsonReturn(-4, '该车暂时处于不可用状态');
            }

            // 查询时间交集
            $hasCarUseData = AdminCarApply::where('car_id', $param['car_id'])
                ->where('start', '<=', $param['end'])
                ->where('end', '>=', $param['start'])
                ->where('status', 2)
                ->find();

            if (!empty($hasCarUseData)) {
                return jsonReturn(-5, '该车在选定的时间内有其他人使用，请调整使用时间。');
            }

            AdminCarApply::insert([
                'admin_id' => Session::get('admin.id'),
                'admin_name' => Session::get('admin.name'),
                'car_id' => $param['car_id'],
                'passengers' => $carInfo['passengers'],
                'car_number' => $carInfo['car_number'],
                'car_type' => $carInfo['car_type'],
                'start_area' => $param['start_area'],
                'end_area' => $param['end_area'],
                'start' => $param['start'],
                'end' => $param['end'],
                'people_num' => $param['people_num'],
                'reason' => $param['reason'],
                'status' => 2, // TODO 接入审核
                'create_time' => date('Y-m-d H:i:s')
            ]);

            return jsonReturn(0, '预约成功');
        }

        $car =  M::select();

        return $this->fetch('', [
            'car' => $car,
            'car_type' => $this->carType,
            'status' => $this->status
        ]);
    }
}
