<?php

declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\admin\controller\Base;
use app\common\model\AdminArticle;
use app\common\model\AdminCate;

class Cate extends Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    public function index()
    {
        if (request()->isAjax()) {

            $list = AdminCate::select();

            return jsonReturn(0, 'success', $list);
        }

        return $this->fetch();
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $param['parent_id'] = $param['select'];
            unset($param['select']);

            $has = AdminCate::where('name', $param['name'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该栏目已经存在');
            }

            $param['create_time'] = date('Y-m-d H:i:s');

            AdminCate::insert($param);

            return jsonReturn(0, '添加成功');
        }

        $cateList = AdminCate::field('cate_id as value,name,parent_id')->where('status', 1)->select()->toArray();

        $cateList = makeTree($cateList);
        $tree[] = [
            'value' => 0,
            'name' => '顶级分类',
            'parent_id' => 0,
            'children' => $cateList
        ];

        return $this->fetch('', [
            'cate' => json_encode($tree)
        ]);
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $param['parent_id'] = $param['select'];
            unset($param['select']);

            $has = AdminCate::where('name', $param['name'])->where('cate_id', '<>', $param['cate_id'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该栏目已经存在');
            }

            $param['update_time'] = date('Y-m-d H:i:s');

            AdminCate::where('cate_id', $param['cate_id'])->update($param);

            return jsonReturn(0, '编辑成功');
        }

        $id = input('param.id');
        $info = AdminCate::where('cate_id', $id)->find();

        if ($info['parent_id'] == 0) {
            $default = [
                'name' => '顶级分类',
                'value' => 0
            ];
        } else {
            $res = AdminCate::where('cate_id', $info['parent_id'])->find();
            $default = [
                'name' => $res['name'],
                'value' => $res['cate_id']
            ];
        }

        $cateList = AdminCate::field('cate_id as value,name,parent_id')->where('status', 1)->select()->toArray();

        $cateList = makeTree($cateList);
        $tree[] = [
            'value' => 0,
            'name' => '顶级分类',
            'parent_id' => 0,
            'children' => $cateList
        ];

        return $this->fetch('', [
            'cate' => json_encode($tree),
            'default' => json_encode($default),
            'info' => $info
        ]);
    }

    public function del()
    {
        $id = input('param.id');

        $has = AdminCate::where('parent_id', $id)->find();
        if (!empty($has)) {
            return jsonReturn(-1, '该分类下有子分类不可删除');
        }

        $has = AdminArticle::where('cate_id', $id)->where('is_delete', 1)->find();
        if (!empty($has)) {
            return jsonReturn(-1, '该分类下有文章不可以删除');
        }

        AdminCate::where('cate_id', $id)->delete();

        return jsonReturn(0, '删除成功');
    }
}