<?php

declare (strict_types = 1);

namespace app\admin\controller\admin;

use app\admin\controller\Base;
use app\common\model\AdminUpdateLog;

class Log extends Base
{
    protected $middleware = ['AdminCheck','AdminPermission'];

    public function index()
    {
        if (request()->isAjax()) {

            $limit = input('param.limit');
            $where = [];

            $logModel = new AdminUpdateLog();
            $list = $logModel->getLogList($where, $limit);

            return json(pageReturn($list));
        }

        return $this->fetch();
    }

    public function add()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $has = AdminUpdateLog::where('version', $param['version'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该版本已经录入，请更换版本');
            }

            $param['create_time'] = date('Y-m-d H:i:s');
            AdminUpdateLog::insert($param);

            return jsonReturn(0, '新增成功');
        }

        return $this->fetch();
    }

    public function edit()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $has = AdminUpdateLog::where('version', $param['version'])->where('log_id', '<>', $param['log_id'])->find();
            if (!empty($has)) {
                return jsonReturn(-1, '该版本已经录入，请更换版本');
            }

            $param['update_time'] = date('Y-m-d H:i:s');
            AdminUpdateLog::where('log_id', $param['log_id'])->update($param);

            return jsonReturn(0, '更新成功');
        }

        $id = input('param.id');

        $info = AdminUpdateLog::where('log_id', $id)->find();
        return $this->fetch('', [
            'info' => $info
        ]);
    }

    public function del()
    {
        $id = input('param.id');

        AdminUpdateLog::where('log_id', $id)->update([
            'is_delete' => 2
        ]);

        return jsonReturn(0, '删除成功');
    }
}